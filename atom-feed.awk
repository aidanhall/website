# Format post links correctly.
function mdlinktohttp(file) {
    return siteurl base "/" gensub(/(md)/, "html", 1, file)
}

# Generate the Atom header.
BEGIN {
    state = "header"
    print "<?xml version=\"1.0\" encoding=\"utf-8\"?>",
        "<feed xmlns=\"http://www.w3.org/2005/Atom\">",
        "<link href=\"" siteurl "\"/>",
        "<author><name>Aidan Hall</name></author>",
        "<id>" siteurl "</id>"
}

# Generate the feed title.
/^# / {
    print "<title>" gensub(/(^# )/, "", 1, $0) " - Aidan's Website</title>"
}

# Set the date for all posts on a day.
match($0, /^\* ([0-9]*)\/([0-9]*)\/([0-9]*):/, dt) {
    pdate = "20" dt[3] "-" dt[2] "-" dt[1] "T"
}

# Generate an entry for the post on the current line.
match($0, /^  \* ([0-9]*):([0-9]*): \[([^]]*)\]\(SSGPAGE\((.*)\)\)/, po) {
    ptime = pdate po[1] ":" po[2] ":00Z"
    if (latest == "") {
        latest = ptime
        print "<updated>" latest "</updated>"
    }
    #print length(po), po[1], po[2], po[3], po[4], po[5], $0
    print "<entry>",
        "<title>" po[3] "</title>",
        "<link href=\"" mdlinktohttp(po[4]) "\"/>",
        "<id>" mdlinktohttp(po[4]) "</id>",
        "<updated>" ptime "</updated>",
        "</entry>"
}

END {
    print "</feed>"
}

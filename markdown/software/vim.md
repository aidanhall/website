# Vim

<span class="blogdate">Originally posted Sat 22 May 17:13:12 2021</span>

Vim is a text editor.
It takes over your life.

FLOATFIG(My Vim Configuration, SITE_URL/pics/vim.webp, width=360px)

## Configuration
Vim makes configuring software necessary, but also _fun._
The consistency of the command mode and plain-text configuration make it easy
to quickly configure new things.

All (sensible) configuration is done using Vim script, an entire programming language
that exists just for configuring Vim.
Plain-text configuration files are simpler to understand and less likely to change
than GUI configuration options,
as well as being less likely to be discarded when the program updates.

## A Tale of Colour Schemes
I remember using PyCharm, an extremely heavy IDE for _Python_ of all things,
and wanting to change the colours of the syntax highlighting
(to make it more like IDLE, which is a poor text editor with bold aspirations).
After diving through menus, I found the relevant section, but it was horribly restrictive
and required me to interpret a pop-up window with millions of buttons and text boxes
to configure the highlighting.

By contrast, in Vim it is as simple as running a command.
For example, this will make variable types (int, float etc.) green:
```{.vim .cb}
highlight Type ctermfg=green guifg=lightgreen gui=none
```

You can put many of these in your `.vimrc` (or equivalent), or better yet,
in a `colorscheme`, which is what I call one of Vim's **useful abstractions.**
My Vim colour scheme is [here](https://gitlab.com/aidanhall/dots/-/blob/master/vim/.config/nvim/colors/aidan.vim).


## Minimal but Useful Abstractions
After a while, one's Vim configuration can balloon to hundreds or thousands of lines.
A solution I came up with was to split mine into multiple files concerning
specific parts of the configuration, such as syntax highlighting and loading plugins,
then to `source` all of the others in the main `init.vim` file.
However, these vague definitions became blurry, and there were still many
`autocmd FileType` commands in the inevitably vague `general-config.vim`.
However, from [this talk](https://youtu.be/Gs1VDYnS-Ac) I discovered how to create
filetype plugins.

Filetype plugins, along with `colorscheme`s, compiler configurations, indentation set-ups
and probably more, are essentially just plain Vim scripts with certain rules
about when they are loaded.
However, this basic level of abstraction naturally leads to a cleaner
configuration when used correctly, since different parts,
right down to individual file types (see below) can be split into separate,
shorter files.
For a sub-optimal, but recently much-improved example,
consider [my own Vim configuration](https://gitlab.com/aidanhall/dots/-/tree/master/vim).

Colour schemes and compilers may be selected interactively or
within a configuration file with the `colorscheme` and `compiler`
commands respectively.
These basically look in certain directories for `*.vim` files,
remove the `.vim` extension, then allow the user to choose one:
```{.vim .cb}
colorscheme aidan
autocmd FileType zig compiler zig_test
```

## File Type Plugins
Vim has a much lower bar for supporting file types with syntax highlighting,
completion, logical object navigation and more, because all that is needed to create
a Vim file type plugin is a little knowledge of Vim script and regular expressions,
which are used to define the language objects for Vim to navigate and highlight.
Consequently,
Vim can easily support many more file types than more complex editors like VS Code.
The installation of Vim 8.2 on my computer has **245** `ftplugin` files
that ship with Vim **by default**, with it being easy to add more by simply
downloading the appropriate files and storing them in your configuration folder.
E.g. `~/.config/nvim/after/ftplugin`.

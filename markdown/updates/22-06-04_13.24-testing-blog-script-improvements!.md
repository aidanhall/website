<article class="blogpost">
# Testing Blog Script Improvements!
<div class="blogdate">Sat  4 Jun 13:24:50 BST 2022</div>

This is just a post to test a couple of minor improvements to the blog
post script.
Post files should now be given meaningful names, so you can get an
idea of what the post is about before reading it.

If I can work out a straightforward way to automate it, I could
potentially apply the same method of filename generation (which uses
the post title) to previous posts.

A simple `sed` expression is used to make the filename all lowercase,
with ASCII hyphens separating the words:

```sh
sed 's/[A-Z]/\l&/g;s/\s/-/g'
```

<!-- Leave a blank line after the last paragraph of the post! -->
</article>

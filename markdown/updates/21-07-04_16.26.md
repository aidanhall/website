<article class="blogpost">
# Blog Advancements
I have made some unreasonably challenging modifications to my blog.

## History Order
The posts are now displayed with the most recent ones first;
this was a simple matter of using sed text insertion.
It is now much easier to access the posts that are most likely to be wanted:
the most recent ones.

## Rolling Log
I have also made a [rolling view](SSGPAGE(rolling.md)),
displaying all of the blog posts.
I have used lazy-loading iframes, which will allow browsers that support them
to only load the contents of each post when it is necessary.
This will improve loading time for the rolling log as the number of posts
increases.

The downside is that iframes were probably not meant for this,
since the entire page (including the footer) must be displayed
for each article.
Ideally, I would statically concatenate all of the article contents
into a single file at build time, but this would remove the benefits of
lazy loading.
Solutions to the other obvious issue, iframe height, also need to be investigated.

</article>

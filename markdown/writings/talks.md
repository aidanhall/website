# Talks

Some talks I have given over the years.

* Talking about Emacs for Only 15 Minutes (18/10/2023):
  [Slides](SITE_URL/docs/uwcs-emacs-talk.pdf),
  [Video](https://youtu.be/n02zSVxQ8Vc?si=SEpKDWGrcSox6yQz).
* Lisp Macros (23/11/2023):
  [Slides](SITE_URL/docs/lisp-macros-presentation.pdf).
* Making Games without an Engine (21/03/2024):
  [Slides](SITE_URL/docs/2024-03-06-no-engine-gamedev.pdf).
* Reproducible System Configuration with Nix and Guix (27/08/2024):
  [Slides](SITE_URL/docs/nix-and-guix.pdf).

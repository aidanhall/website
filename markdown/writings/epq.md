# EPQ: Creating a Robot that Models Asimov's Three Laws of Robotics

<span class="blogdate">19/02/2021</span>

[PDF](SITE_URL/docs/epq-writeup.pdf)

FLOATFIG(The Robot,SITE_URL/pics/epq-robot.jpg,)

This was my A-level
[Extended Project Qualification](https://www.aqa.org.uk/subjects/projects/project-qualifications/EPQ-7993/introduction)
project, which received an A grade.
Through it I developed and applied knowledge of embedded systems
design, computer vision, mobile robotics, and artificial intelligence.

## Introduction

In the modern world, we rely on computer-controlled systems to make
our lives easier. Such systems can be incredibly complex, which can
lead to oversights, resulting in failures that caused the tragic 737
plane crashes in 2019 (FAA 2019).

I believe that it could be helpful to create a simple model for how
these systems could operate safely, which could potentially be used as
a basis for more complex designs in the future.

This project is about creating a robot, and finding ways to integrate
eth- ical decision making into its design, so it operates safely, as
well as thinking about who is responsible for the decisions it then
makes. The small scale of the project will make it possible to have a
somewhat complete understanding of the full system, and so to create a
basic model for how ethical (i.e. safe) computer systems can be
designed.

I will use Asimov’s Laws (Asimov 1950) to model the ethical system,
since they are an appropriately simple model of ethics to complement
my simple robot. I will also considering their limitations.

# BrainFuck Compiler

I recently wrote
[a BrainFuck (BF) compiler](
https://gitlab.com/aidanhall/titanic-bf-compiler),
targeting x86 assembly with NASM syntax.
It is mostly functional, except for a few corner cases with the jump
instructions that I couldn't figure out, but I decided I'd derived all
the educational value I was going to from the experience.

## Compiler Architecture

Since I was doing this for my own learning, I somewhat deliberately
over-engineered the solution, with explicit lexical analysis, parsing,
optimisation and code generation stages, implemented as functions.

## Lexical Analysis

A valuable quality of BF as an exercise for implementation is that,
since it is comprised of a sequence of 1-character tokens, the lexical
analysis stage is completely trivial.

## Parsing (and Rust Stuff)

Having previously used Haskell with the wonderful megaparsec library
to implement an interpreter for a simple C-like language, I understood
how naturally algebraic data types (`enum`s in Rust) could be used to
construct an abstract syntax tree.

Pattern matching on my `Token` and `Symbol` types, as well as Rust's
`Result` and `Option` types led to multiple cases where the
implementations for otherwise awkward behaviours were easy.
For example, normally, when a `Token` is parsed, a `Symbol` is
produced, which can then be added to the current `Ast`.
However, when parsing a `BracketLeft` `Token`, instead of adding a
`Symbol` to the `Ast`, a new, nested `Ast` is pushed onto a stack.
This behaviour is encapsulated by matching on the `Token`, and
producing an `Option`, with an `if let` pattern used to add a `Symbol`
only in those cases where one is produced.

In a language like C, I might have needed to use a flag variable to
indicate when a new `Symbol` was produced, or use a `continue`
statement in the `BracketLeft` branch. These would have involved more
explicit control flow to keep track of, somewhat increasing the
cognitive load of reading the implementation.
Admittedly, those alternatives wouldn't have been so bad, and would
have worked in Rust, but the solution I came up with feels more like
“idiomatic Rust”, although I'm not experienced enough to really know
what that is.

Implementing the parser (`parsed()`) also forced me to gain a better
understanding of ownership in Rust, concerning the moving of ownership
of `Ast` objects between the stack, the `Loop` nesting symbols, and
the function's caller (when returning).

## Assembly Nonsense

I had done a tiny amount of assembly programming before, but
implementing BF's computational model gave me some nice experience
dealing with register management, as well as other niche concerns like
indexed addressing and the register value stack.

Since my program automatically generates the code anyway, I probably
didn't need to use macros or subroutines, but doing so did mean that
the mapping to x86 for each BF instruction ended up being comfortably
short.

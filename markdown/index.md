# Homepage
## Who am I?  What is this place?
FLOATFIG(Me, SITE_URL/pics/moon-silhouette-aidan.webp, class="circle")

To you, I am a stranger on the internet with an interest in computer
science and programming.

The "Subdir Root" link goes to `index.html` in the current
subdirectory.

## Here

<div class="index-spread">

<section>
### Index

* [Blog](SSGPAGE(blog/index.md))
* [Writings](SSGPAGE(writings/index.md))
* [Quotes](SSGPAGE(misc/quotes.md))
* [Miscellaneous](SSGPAGE(misc/index.md))
* [Talks](SSGPAGE(writings/talks.md))
</section>

<section>
### Programming

* [Portfolio](SSGPAGE(projects/index.md))
* [GitHub](https://www.github.com/aidan-hall)
* [GitLab](https://www.gitlab.com/aidanhall)
* [Itch.io](https://aidan-hall.itch.io/) (games)
</section>

<section>
### My Stuff

* [Software I Use](SSGPAGE(software/index.md))
* [Personal Page](SSGPAGE(personal/index.md))
* [SSGSS](SSGPAGE(ssgss.md) "Static Site Generator Super Simple")
* [Site Updates](SSGPAGE(updates/index.md))
</section>

</div>

## Elsewhere

### Blogs

I do not necessarily agree with any of the content on these sites, but
I find them interesting.

* [Emacs Redux](https://emacsredux.com/archive/): Emacs, Emacs and
  more Emacs.
* [Eric Murphy](https://ericmurphy.xyz/): Compelling thoughts about
  the web, from a web developer.
* [null program](https://nullprogram.com/index/): Posts about most of
  tech, going back to the dawn of time (2007).
* [The Pasture](https://thephd.dev/): Accounts of epic campaigns to
  make C and C++ suck a bit less, and ways of coping with the fact
  that they do.

### Papers

For clarity, not written by me, but ones I have found especially
interesting.

* [C Is Not a Low-level Language](https://queue.acm.org/detail.cfm?id=3212479)
* [To Write Code: The Cultural Fabrication of Programming Notation and Practice](https://dl.acm.org/doi/abs/10.1145/3313831.3376731)
* [An Efficiency Comparison of Document Preparation Systems Used in Academic Research and Development](https://dx.plos.org/10.1371/journal.pone.0115069)
    

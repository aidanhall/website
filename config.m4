m4_define(`SITENAME', Aidan's Website)
m4_define(`AUTHORNAME', `Aidan Hall')

m4_define(LBRY_INVITE, https://odysee.com/$/invite/@Argletrough:6)
m4_define(SITE_URL, https://aidanhall.gitlab.io)
m4_define(STYLESHEET, SITE_URL/stylesheet.css)

m4_define(`FLOATFIG',<figure class="rfloat">
<a href="$2" target=_blank><img src="$2" alt="$1" title="$1" $3></a>
<figcaption>$1</figcaption>
</figure>)
m4_define(`LATEX', L<sup>A</sup>T<sub>E</sub>X)

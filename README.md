# Static Site Generator Super Simple
> You see Zuckerberg, you aren't dealing with the average static site generator any more...
* This is either the source tree or a fork of SSGSS by Aidan Hall.
* Inspired primarily in name and the concept of using a Makefile by ssg5, but way worse, probably.
* This site uses `m4 -P` for preprocessing.
* Files in the MDROOTDIR directory are formatted with m4 and markdown.
* Files in the RAWDIR directory are copied directly into the website directory.
* `<title>` contents determined with `sed 's/^#*\s*//;q'`.

<!-- -*- mode: html; -*- -->
</main>
<nav>
<ul id="navigation">
<li><a href="SITE_URL" >&#127968; Home</a></li>
<li><a href="SITE_URL/blog/" >&#128212; Blog</a></li>
<li><a href="./" >&#12106; Subdir Root</a></li>
</ul></nav>
<footer>
<p>
&copy; AUTHORNAME 2021-2023.
Last <abbr title="Most recent Git commit.">change</abbr>:
COMMITDATE.
<a href="https://gitlab.com/aidanhall/ssgss"
   title='Using "Static Site Converter Super Simple"'>Compiled</a>
on DATE.
<a href="#top">🔝</a>

</footer>
</body></html>
